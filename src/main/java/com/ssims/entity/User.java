package com.ssims.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName User
 * @Descrition 用户实体类，默认继承策略为单表
 * @Author Jackie
 * @Date 2018/12/23 22:53
 */
@Data
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "t_user")
public class User implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -599302865579046795L;
    /**
     * id
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    /**
     * 姓名
     */
    @Column(nullable = false)
    protected String name;
    /**
     * 登录帐号
     */
    @Column(unique = true)
    protected String account;
    /**
     * 编号
     */
    @Column(unique = true, nullable = false)
    protected String num;
    /**
     * 密码，反序列化时只可写，序列化时不可读
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    protected String password;
    /**
     * 状态
     */
    @Enumerated(EnumType.STRING)
    protected UserStateEnum state;
    /**
     * 创建时间
     */
    protected Date createTime;
    /**
     * 更新时间
     */
    protected Date updateTime;
    /**
     * 最后登录时间
     */
    protected Date lastLoginTime;
    /**
     * 用户头像
     */
    protected String profilePhoto = "/images/user/default-profile.png";
    /**
     * 性别
     */
    @Enumerated(EnumType.STRING)
    protected GenderEnum gender;
    /**
     * 年龄
     */
    protected Integer age;
    /**
     * 出生日期
     */
    @Temporal(TemporalType.DATE)
    protected Date birthday;
    /**
     * 手机号码
     */
    protected String phone;
    /**
     * 邮箱
     */
    protected String email;
    /**
     * 住址
     */
    protected String address;
    /**
     * 子类的鉴别器，存储关联的子类名称，用于区分属于不同子类类型的行
     */
    @Column(name = "dtype", insertable = false, updatable = false)
    protected String type;

    @PrePersist
    protected void initializeUser() {
        this.updateTime = this.createTime = new Date();
        if (this.state == null) {
            this.setState(UserStateEnum.DISABLED);
        }
    }

    @PreUpdate
    protected void updateUser() {
        this.updateTime = new Date();
    }
}
