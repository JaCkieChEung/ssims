package com.ssims.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName WebMvcConfig
 * @Descrition WebMvcConfig配置类
 * @Author Jackie
 * @Date 2018/12/26 15:15
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        /* 登录页 */
        registry.addViewController("/").setViewName("login");
        /* 主页 */
        registry.addViewController("/index.html").setViewName("index");
        /* 学生信息管理 */
        registry.addViewController("/add-stu.html").setViewName("add-stu");
        registry.addViewController("/stu-list.html").setViewName("stu-list");
        registry.addViewController("/stu-detail.html").setViewName("stu-detail");
        /* 课程信息管理 */
        registry.addViewController("/add-course.html").setViewName("add-course");
        registry.addViewController("/course-list.html").setViewName("course-list");
        registry.addViewController("/course-detail.html").setViewName("course-detail");
        /* 成绩信息管理 */
        registry.addViewController("/add-score.html").setViewName("add-score");
        registry.addViewController("/score-list.html").setViewName("score-list");
        registry.addViewController("/score-detail.html").setViewName("score-detail");
    }
}
