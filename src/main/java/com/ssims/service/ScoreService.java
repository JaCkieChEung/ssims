package com.ssims.service;

import com.ssims.common.JsonResponse;
import com.ssims.entity.Score;
import com.ssims.model.ScoreDTO;

import java.util.Optional;

/**
 * @ClassName ScoreService
 * @Descrition 成绩业务逻辑接口
 * @Author Jackie
 * @Date 2018/12/22 15:55
 */
public interface ScoreService extends BaseService<Score, Long> {
    /**
     * 新增或修改成绩
     *
     * @param scoreDTO Score数据传输对象
     * @return JsonResponse
     */
    JsonResponse addOrUpdateScore(ScoreDTO scoreDTO);

    /**
     * 根据学号和课程号查询成绩
     *
     * @param sno 学号
     * @param cno 课程号
     * @return Optional<Score>
     */
    Optional<Score> findBySnoAndCno(String sno, String cno);
}
