package com.ssims.service;

import com.ssims.common.JsonResponse;
import com.ssims.entity.Course;
import com.ssims.model.CourseDTO;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName CourseService
 * @Descrition 课程业务逻辑接口
 * @Author Jackie
 * @Date 2018/12/22 15:55
 */
public interface CourseService extends BaseService<Course, Long> {
    /**
     * 根据课程号查询课程
     *
     * @param cno 课程号
     * @return Optional<Student>
     */
    Optional<Course> findByCno(String cno);

    /**
     * 新增或修改课程
     *
     * @param courseDTO Course数据传输对象
     * @return JsonResponse
     */
    JsonResponse addOrUpdateCourse(CourseDTO courseDTO);

    /**
     * 查询所有课程号
     *
     * @return List<Object>
     */
    List<Object> findAllCno();
}
