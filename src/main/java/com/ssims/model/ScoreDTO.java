package com.ssims.model;

import com.ssims.common.StringUtils;
import com.ssims.entity.Score;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ScoreDTO
 * @Descrition Score数据传输对象，封装前端传递给后端的数据
 * @Author Jackie
 * @Date 2018/12/23 0:15
 */
@Data
@NoArgsConstructor
public class ScoreDTO {
    private Long id;

    @NotBlank(message = "学号不能为空！")
    private String sno;

    @NotBlank(message = "课程号不能为空！")
    private String cno;

    @NotNull(message = "成绩不能为空！")
    private Double grade;

    /**
     * 关键字
     */
    private String keyword;

    public static <T extends Score> Specification<T> getScoreSpecification(ScoreDTO scoreDTO) {
        return new Specification<T>() {

            private static final long serialVersionUID = -6625274136090636216L;

            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                /* 1.Predicate查询条件集合 */
                List<Predicate> predicates = new ArrayList<>();

                /* 2.根据DTO的数据字段的值进行判断以及条件的组装 */
                if (StringUtils.isNotBlank(scoreDTO.getKeyword())) {
                    predicates.add(criteriaBuilder.or(
                            criteriaBuilder.like(root.get("course").get("cno").as(String.class),
                                    "%" + scoreDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("course").get("cname").as(String.class),
                                    "%" + scoreDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("student").get("num").as(String.class),
                                    "%" + scoreDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("student").get("name").as(String.class),
                                    "%" + scoreDTO.getKeyword() + "%")));
                }

                /* 3.根据Predicate查询条件集合的size创建对应的Predicate查询条件数组 */
                Predicate[] pre = new Predicate[predicates.size()];

                /* 4.使用CriteriaBuilder的and函数组装查询条件数组 */
                return query.where(predicates.toArray(pre)).getRestriction();
            }
        };
    }
}
