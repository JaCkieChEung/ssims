package com.ssims.model;

import com.ssims.common.StringUtils;
import com.ssims.entity.Course;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName CourseDTO
 * @Descrition Course数据传输对象，封装前端传递给后端的数据
 * @Author Jackie
 * @Date 2018/12/23 0:17
 */
@Data
@NoArgsConstructor
public class CourseDTO {
    private Long id;

    @NotBlank(message = "课程号不能为空！")
    private String cno;

    @NotBlank(message = "课程名称不能为空！")
    private String cname;

    private Double credit;

    private String teacherNo;

    /**
     * 关键字
     */
    private String keyword;

    public static <T extends Course> Specification<T> getCourseSpecification(CourseDTO courseDTO) {
        return new Specification<T>() {

            private static final long serialVersionUID = 8670001579615867105L;

            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                /* 1.Predicate查询条件集合 */
                List<Predicate> predicates = new ArrayList<>();

                /* 2.根据DTO的数据字段的值进行判断以及条件的组装 */
                if (StringUtils.isNotBlank(courseDTO.getKeyword())) {
                    predicates.add(criteriaBuilder.or(
                            criteriaBuilder.like(root.get("cno").as(String.class),
                                    "%" + courseDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("cname").as(String.class),
                                    "%" + courseDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("teacherNo").as(String.class),
                                    "%" + courseDTO.getKeyword() + "%")));
                }

                /* 3.根据Predicate查询条件集合的size创建对应的Predicate查询条件数组 */
                Predicate[] pre = new Predicate[predicates.size()];

                /* 4.使用CriteriaBuilder的and函数组装查询条件数组 */
                return query.where(predicates.toArray(pre)).getRestriction();
            }
        };
    }
}
