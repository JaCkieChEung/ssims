package com.ssims.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName CourseRepository
 * @Descrition 课程实体类
 * @Author Jackie
 * @Date 2018/12/21 22:30
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "t_course")
public class Course implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 4995722783882199038L;
    /**
     * id
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 课程号
     */
    @Column(unique = true, nullable = false)
    private String cno;
    /**
     * 课程名称
     */
    @Column(nullable = false)
    private String cname;
    /**
     * 学分
     */
    @Column(columnDefinition = "NUMBER(10,1) default '0.0'")
    private Double credit;
    /**
     * 教师编号
     */
    private String teacherNo;
    /**
     * 创建时间
     */
    private Date createTime;
    @PrePersist
    protected void initializeCourse() {
        this.updateTime = this.createTime = new Date();
    }
    /**
     * 更新时间
     */
    private Date updateTime;
    @PreUpdate
    protected void updateCourse() {
        this.updateTime = new Date();
    }
}
