package com.ssims.service.impl;

import com.ssims.common.BeanUtils;
import com.ssims.common.JsonResponse;
import com.ssims.common.StringUtils;
import com.ssims.entity.Score;
import com.ssims.model.ScoreDTO;
import com.ssims.model.ScoreVO;
import com.ssims.repository.CourseRepository;
import com.ssims.repository.ScoreRepository;
import com.ssims.repository.StudentRepository;
import com.ssims.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @ClassName ScoreServiceImpl
 * @Descrition 成绩业务逻辑接口实现类
 * @Author Jackie
 * @Date 2018/12/22 15:57
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ScoreServiceImpl extends BaseServiceImpl<Score, Long> implements ScoreService {

    private final ScoreRepository scoreRepository;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public ScoreServiceImpl(ScoreRepository scoreRepository, StudentRepository studentRepository, CourseRepository courseRepository) {
        super.setBaseRepository(scoreRepository);
        this.scoreRepository = scoreRepository;
        this.studentRepository = studentRepository;
        this.courseRepository = courseRepository;
    }

    /**
     * 新增或修改成绩
     *
     * @param scoreDTO Score数据传输对象
     * @return JsonResponse
     */
    @Override
    public JsonResponse addOrUpdateScore(ScoreDTO scoreDTO) {
        Long scoreId = scoreDTO.getId();
        final Score[] score = {null};
        String sno = scoreDTO.getSno();
        String cno = scoreDTO.getCno();
        JsonResponse result = new JsonResponse();

        try {
            if (StringUtils.isNotBlank(sno) && !studentRepository.findByNum(sno).isPresent()) {
                return result.buildErrorResult("成绩保存失败！学生学号不存在！");
            }
            if (StringUtils.isNotBlank(cno) && !courseRepository.findByCno(cno).isPresent()) {
                return result.buildErrorResult("成绩保存失败！课程号不存在！");
            }
            if (StringUtils.isNotBlank(sno) && StringUtils.isNotBlank(cno) && scoreRepository.findBySnoAndCno(sno, cno).isPresent() && !scoreRepository.findBySnoAndCno(sno, cno).get().getId().equals(scoreId)) {
                return result.buildErrorResult("成绩保存失败！该学生成绩已存在！");
            }
            // id为空，新增成绩
            if (scoreId == null) {
                score[0] = new Score();
            } else {
                if (!this.existsById(scoreId)) {
                    return result.buildErrorResult("成绩id不存在，成绩修改失败！");
                } else {
                    scoreRepository.findById(scoreId).ifPresent(s -> score[0] = s);

                }
            }
            BeanUtils.copyProperties(scoreDTO, score[0]);
            studentRepository.findByNum(sno).ifPresent(s -> {
                if (score[0] != null) {
                    score[0].setStudent(s);
                }
            });
            courseRepository.findByCno(cno).ifPresent(c -> {
                if (score[0] != null) {
                    score[0].setCourse(c);
                }
            });
            return result.buildSuccessResult(new ScoreVO(scoreRepository.save(score[0])), "成绩保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return result.buildErrorResult(e.getMessage());
        }
    }

    /**
     * 根据学号和课程号查询成绩
     *
     * @param sno 学号
     * @param cno 课程号
     * @return Optional<Score>
     */
    @Override
    public Optional<Score> findBySnoAndCno(String sno, String cno) {
        return scoreRepository.findBySnoAndCno(sno, cno);
    }

    public ScoreRepository getScoreRepository() {
        return scoreRepository;
    }

    public CourseRepository getCourseRepository() {
        return courseRepository;
    }

    public StudentRepository getStudentRepository() {
        return studentRepository;
    }
}