package com.ssims.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ssims.common.BeanUtils;
import com.ssims.entity.Score;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName ScoreVO
 * @Descrition Score视图对象，封装后端返回给前端的数据
 * @Author Jackie
 * @Date 2018/12/23 0:18
 */
@Data
@NoArgsConstructor
public class ScoreVO {
    /**
     * id
     */
    private Long id;
    /**
     * 学生学号
     */
    private String sno;
    /**
     * 学生姓名
     */
    private String sname;
    /**
     * 课程号
     */
    private String cno;
    /**
     * 课程名称
     */
    private String cname;
    /**
     * 成绩
     */
    private Double grade;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    public ScoreVO(Score score) {
        BeanUtils.copyProperties(score, this);
        if (score.getStudent() != null) {
            if (score.getStudent().getNum() != null) {
                this.sno = score.getStudent().getNum();
            }
            if (score.getStudent().getName() != null) {
                this.sname = score.getStudent().getName();
            }
        }
        if (score.getCourse() != null) {
            if (score.getCourse().getCno() != null) {
                this.cno = score.getCourse().getCno();
            }
            if (score.getCourse().getCname() != null) {
                this.cname = score.getCourse().getCname();
            }
        }
    }
}
