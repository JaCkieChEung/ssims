package com.ssims.repository;

import com.ssims.entity.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName CourseRepository
 * @Descrition 课程数据访问接口
 * @Author Jackie
 * @Date 2018/12/21 22:43
 */
@Repository
public interface CourseRepository extends BaseRepository<Course, Long> {
    /**
     * 根据课程号查询课程
     *
     * @param cno 课程号
     * @return Optional<Course>
     */
    Optional<Course> findByCno(String cno);

    /**
     * 查询所有课程号
     *
     * @return List<Object>
     */
    @Query("select c.cno, c.cname from Course c")
    List<Object> findAllCno();
}
