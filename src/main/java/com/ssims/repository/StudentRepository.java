package com.ssims.repository;

import com.ssims.entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName StudentRepository
 * @Descrition 学生数据访问接口
 * @Author Jackie
 * @Date 2018/12/21 22:42
 */
@Repository
public interface StudentRepository extends BaseRepository<Student, Long> {
    /**
     * 根据学号查询学生
     *
     * @param num 学号
     * @return Optional<Student>
     */
    Optional<Student> findByNum(String num);

    /**
     * 查询所有学号
     *
     * @return List<Object>
     */
    @Query("select s.num, s.name from Student s")
    List<Object> findAllSno();


}
