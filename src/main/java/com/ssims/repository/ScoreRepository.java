package com.ssims.repository;

import com.ssims.entity.Score;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @ClassName ScoreRepository
 * @Descrition 成绩数据访问接口
 * @Author Jackie
 * @Date 2018/12/21 22:43
 */
@Repository
public interface ScoreRepository extends BaseRepository<Score, Long> {

    /**
     * 根据学号和课程号查询成绩
     *
     * @param sno 学号
     * @param cno 课程号
     * @return Optional<Score>
     */
    @Query("from Score s where s.student.num = :sno and s.course.cno = :cno")
    Optional<Score> findBySnoAndCno(@Param("sno") String sno, @Param("cno") String cno);
}
