package com.ssims.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @ClassName Student
 * @Descrition 学生实体类
 * @Author Jackie
 * @Date 2018/12/21 15:30
 */
@Data
@NoArgsConstructor
@Entity
@JsonIgnoreProperties(value = { "scores" })
public class Student extends User {
    /**
     * uuid
     */
    private static final long serialVersionUID = -7148030653561243616L;
    /**
     * 专业
     */
    @Column(nullable = false)
    private String speciality;
    /**
     * 班级
     */
    @Column(nullable = false)
    private String sclass;
    /**
     * 总学分
     */
    @Column(columnDefinition = "NUMBER(10,1) default '0.0'")
    private Double totalCredit;
    /**
     * 成绩
     */
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private List<Score> scores;

    @PrePersist
    protected void initializeStudent() {
        super.initializeUser();
    }

    @PreUpdate
    protected void updateStudent() {
        super.updateUser();
    }
}
