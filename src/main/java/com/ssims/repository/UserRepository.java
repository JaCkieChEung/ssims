package com.ssims.repository;

import com.ssims.entity.User;

import java.util.Optional;

/**
 * @ClassName UserRepository
 * @Descrition 用户数据访问接口
 * @Author Jackie
 * @Date 2018/12/28 15:10
 */
public interface UserRepository extends BaseRepository<User, Long> {
    /**
     * 根据帐号和密码查找用户
     *
     * @param account  帐号
     * @param password 密码
     * @return Optional<User>
     */
    Optional<User> findByAccountAndPassword(String account, String password);
}
