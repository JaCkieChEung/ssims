package com.ssims.controller;

import com.ssims.common.JsonResponse;
import com.ssims.common.PageableBuilder;
import com.ssims.entity.Course;
import com.ssims.model.CourseDTO;
import com.ssims.model.CourseVO;
import com.ssims.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

/**
 * @ClassName CourseController
 * @Descrition Course控制器
 * @Author Jackie
 * @Date 2018/12/23 0:20
 */
@RestController
@RequestMapping("/course")
public class CourseController {
    private final CourseService courseServiceImpl;

    @Autowired
    public CourseController(CourseService courseServiceImpl) {
        this.courseServiceImpl = courseServiceImpl;
    }

    /**
     * 增加或修改课程
     *
     * @param courseDTO     CourseDTO
     * @param bindingResult 验证结果
     * @return Map<String, Object>
     */
    @PostMapping("/save")
    public Map<String, Object> saveCourse(@RequestBody @Valid CourseDTO courseDTO, BindingResult bindingResult) {
        try {
            // 参数不合法，返回错误提示信息
            if (bindingResult.hasErrors()) {
                return JsonResponse.getErrorResult("参数不合法！" + bindingResult.getAllErrors().get(0).getDefaultMessage());
            } else {
                return courseServiceImpl.addOrUpdateCourse(courseDTO).toMap();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("课程保存失败！" + e.getMessage());
        }
    }

    /**
     * 修改课程信息
     *
     * @param id        需要修改的课程的id
     * @param courseDTO CourseDTO
     * @return Map<String, Object>
     */
    @PutMapping("/update/{id}")
    public Map<String, Object> updateCourse(@PathVariable("id") Long id, @RequestBody @Valid CourseDTO courseDTO, BindingResult bindingResult) {
        try {
            // 参数不合法，返回错误提示信息
            if (bindingResult.hasErrors()) {
                return JsonResponse.getErrorResult("参数不合法！" + bindingResult.getAllErrors().get(0).getDefaultMessage());
            } else {
                courseDTO.setId(id);
                return courseServiceImpl.addOrUpdateCourse(courseDTO).toMap();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("课程信息修改失败！" + e.getMessage());
        }
    }

    /**
     * 查询所有课程
     *
     * @param courseDTO       CourseDTO
     * @param pageableBuilder 分页构造器
     * @return Map<String, Object>
     */
    @GetMapping("/all")
    public Map<String, Object> findAllCourse(CourseDTO courseDTO, PageableBuilder pageableBuilder) {
        try {
            return JsonResponse.getSuccessResult(courseServiceImpl.findAll(CourseDTO.getCourseSpecification(courseDTO), pageableBuilder).map(CourseVO::new), "分页条件查询所有课程成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("分页查询所有课程失败！" + e.getMessage());
        }
    }

    /**
     * 查询课程详情
     *
     * @param cId 课程id
     * @return Map<String, Object>
     */
    @RequestMapping("/detail/{cId}")
    public Map<String, Object> findById(@PathVariable("cId") Long cId) {
        try {
            Optional<Course> optCourse = courseServiceImpl.findById(cId);
            if (optCourse.isPresent()) {
                return JsonResponse.getSuccessResult(new CourseVO(optCourse.get()), "查询课程详情成功！");
            } else {
                return JsonResponse.getErrorResult("该课程不存在！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("查询课程详情失败！" + e.getMessage());
        }
    }

    /**
     * 删除所选课程
     *
     * @param ids 需要删除的课程的id
     * @return Map<String, Object>
     */
    @RequestMapping("/delete")
    public Map<String, Object> deleteCourse(@RequestParam(value="ids[]") Long[] ids) {
        try {
            courseServiceImpl.deleteByIds(ids);
            return JsonResponse.getSuccessResult("所选课程删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("所选课程删除失败！" + e.getMessage());
        }
    }

    /**
     * 查询所有课程号
     *
     * @return Map<String, Object>
     */
    @RequestMapping("/all/cno")
    public Map<String, Object> findAllCno() {
        try {
            return JsonResponse.getSuccessResult(courseServiceImpl.findAllCno(), "查询所有课程号成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("查询所有课程号失败！" + e.getMessage());
        }
    }

    public CourseService getCourseServiceImpl() {
        return courseServiceImpl;
    }
}
