package com.ssims.model;

import com.ssims.common.StringUtils;
import com.ssims.entity.GenderEnum;
import com.ssims.entity.Student;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName StudentDTO
 * @Descrition Student数据传输对象，封装前端传递给后端的数据
 * @Author Jackie
 * @Date 2018/12/23 0:17
 */
@Data
@NoArgsConstructor
public class StudentDTO {
    /**
     * id
     */
    private Long id;
    /**
     * 姓名
     */
    @NotBlank(message = "学生姓名不能为空！")
    private String name;
    /**
     * 学号
     */
    @NotBlank(message = "学生学号不能为空！")
    private String num;
    /**
     * 性别
     */
    @NotNull(message = "学生性别不能为空！")
    private GenderEnum gender;
    /**
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    /**
     * 专业
     */
    @NotBlank(message = "学生专业不能为空！")
    private String speciality;
    /**
     * 班级
     */
    @NotBlank(message = "学生班级不能为空！")
    private String sclass;
    /**
     * 总学分
     */ 
    private Double totalCredit;
    /**
     * 成绩
     */
    private List<Long> scoreIds;
    /**
     * 关键字
     */
    private String keyword;

    public static <T extends Student> Specification<T> getStudentSpecification(StudentDTO studentDTO) {
        return new Specification<T>() {
            private static final long serialVersionUID = -6767827745593079905L;

            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                /* 1.Predicate查询条件集合 */
                List<Predicate> predicates = new ArrayList<>();

                /* 2.根据DTO的数据字段的值进行判断以及条件的组装 */
                if (StringUtils.isNotBlank(studentDTO.getKeyword())) {
                    predicates.add(criteriaBuilder.or(
                            criteriaBuilder.like(root.get("num").as(String.class),
                                    "%" + studentDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("name").as(String.class),
                                    "%" + studentDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("speciality").as(String.class),
                                    "%" + studentDTO.getKeyword() + "%"),
                            criteriaBuilder.like(root.get("sclass").as(String.class),
                                    "%" + studentDTO.getKeyword() + "%")));
                }

                /* 3.根据Predicate查询条件集合的size创建对应的Predicate查询条件数组 */
                Predicate[] pre = new Predicate[predicates.size()];

                /* 4.使用CriteriaBuilder的and函数组装查询条件数组 */
                return query.where(predicates.toArray(pre)).getRestriction();
            }
        };
    }


}
