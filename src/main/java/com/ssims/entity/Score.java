package com.ssims.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName ScoreRepository
 * @Descrition 成绩实体类
 * @Author Jackie
 * @Date 2018/12/21 22:35
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "t_score")
public class Score implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -4496004273444327361L;
    /**
     * id
     */
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 学生
     */
    @ManyToOne
    @JoinColumn(name = "stu_id")
    private Student student;
    /**
     * 课程
     */
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;
    /**
     * 成绩
     */
    @Column(columnDefinition = "NUMBER(10,1) default '0.0'")
    private Double grade;
    /**
     * 创建时间
     */
    private Date createTime;
    @PrePersist
    protected void initializeScore() {
        this.updateTime = this.createTime = new Date();
    }
    /**
     * 更新时间
     */
    private Date updateTime;
    @PreUpdate
    protected void updateScore() {
        this.updateTime = new Date();
    }
}
