package com.ssims.controller;

import com.ssims.common.JsonResponse;
import com.ssims.common.PageableBuilder;
import com.ssims.entity.Student;
import com.ssims.model.StudentDTO;
import com.ssims.model.StudentVO;
import com.ssims.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

/**
 * @ClassName StudentController
 * @Descrition Student控制器
 * @Author Jackie
 * @Date 2018/12/23 0:19
 */
@RestController
@RequestMapping("/stu")
public class StudentController {

    private final StudentService studentServiceImpl;

    @Autowired
    public StudentController(StudentService studentServiceImpl) {
        this.studentServiceImpl = studentServiceImpl;
    }

    /**
     * 增加或修改学生
     *
     * @param studentDTO    StudentDTO
     * @param bindingResult 验证结果
     * @return Map<String, Object>
     */
    @PostMapping("/save")
    public Map<String, Object> saveStu(@RequestBody @Valid StudentDTO studentDTO, BindingResult bindingResult) {
        try {
            // 参数不合法，返回错误提示信息
            if (bindingResult.hasErrors()) {
                return JsonResponse.getErrorResult("参数不合法！" + bindingResult.getAllErrors().get(0).getDefaultMessage());
            } else {
                return studentServiceImpl.addOrUpdateStu(studentDTO).toMap();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("学生保存失败！" + e.getMessage());
        }
    }

    /**
     * 修改学生信息
     *
     * @param id         需要修改的学生的id
     * @param studentDTO StudentDTO
     * @return Map<String, Object>
     */
    @PutMapping("/update/{id}")
    public Map<String, Object> updateStu(@PathVariable("id") Long id, @RequestBody @Valid StudentDTO studentDTO, BindingResult bindingResult) {
        try {
            // 参数不合法，返回错误提示信息
            if (bindingResult.hasErrors()) {
                return JsonResponse.getErrorResult("参数不合法！" + bindingResult.getAllErrors().get(0).getDefaultMessage());
            } else {
                studentDTO.setId(id);
                return studentServiceImpl.addOrUpdateStu(studentDTO).toMap();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("学生信息修改失败！" + e.getMessage());
        }
    }

    /**
     * 查询所有学生
     *
     * @param studentDTO      StudentDTO
     * @param pageableBuilder 分页构造器
     * @return Map<String, Object>
     */
    @GetMapping("/all")
    public Map<String, Object> findAllStu(StudentDTO studentDTO, PageableBuilder pageableBuilder) {
        try {
            return JsonResponse.getSuccessResult(studentServiceImpl.findAll(StudentDTO.getStudentSpecification(studentDTO), pageableBuilder).map(StudentVO::new), "分页条件查询所有学生成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("分页查询所有学生失败！" + e.getMessage());
        }
    }

    /**
     * 查询学生详情
     *
     * @param stuId 学生id
     * @return Map<String, Object>
     */
    @RequestMapping("/detail/{stuId}")
    public Map<String, Object> findById(@PathVariable("stuId") Long stuId) {
        try {
            Optional<Student> optStu = studentServiceImpl.findById(stuId);
            if (optStu.isPresent()) {
                return JsonResponse.getSuccessResult(new StudentVO(optStu.get()), "查询学生详情成功！");
            } else {
                return JsonResponse.getErrorResult("该学生不存在！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("查询学生详情失败！" + e.getMessage());
        }
    }

    /**
     * 删除所选学生
     *
     * @param ids 需要删除的学生的id，后台接收前端传过来的数组需要加@RequestParam(value="ids[]")，
     *            注意：这里必须写value=ids[]，并且必须带中阔号。
     * @return Map<String, Object>
     */
    @RequestMapping("/delete")
    public Map<String, Object> deleteStu(@RequestParam(value="ids[]") Long[] ids) {
        try {
            studentServiceImpl.deleteByIds(ids);
            return JsonResponse.getSuccessResult("所选学生删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("所选学生删除失败！" + e.getMessage());
        }
    }

    /**
     * 查询所有学号
     *
     * @return Map<String, Object>
     */
    @RequestMapping("/all/sno")
    public Map<String, Object> findAllSno() {
        try {
            return JsonResponse.getSuccessResult(studentServiceImpl.findAllSno(), "查询所有学号成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("查询所有学号失败！" + e.getMessage());
        }
    }

    public StudentService getStudentServiceImpl() {
        return studentServiceImpl;
    }
}
