package com.ssims.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ssims.common.BeanUtils;
import com.ssims.entity.GenderEnum;
import com.ssims.entity.Student;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName StudentVO
 * @Descrition Student视图对象，封装后端返回给前端的数据
 * @Author Jackie
 * @Date 2018/12/23 0:18
 */
@Data
@NoArgsConstructor
public class StudentVO {
    /**
     * id
     */
    private Long id;
    /**
     * 学号
     */
    private String num;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别
     */
    private GenderEnum gender;
    /**
     * 出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birthday;
    /**
     * 专业
     */
    private String speciality;
    /**
     * 班级
     */
    private String sclass;
    /**
     * 总学分
     */
    private Double totalCredit;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    public StudentVO(Student student) {
        BeanUtils.copyProperties(student, this);
    }
}
