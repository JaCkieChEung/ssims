package com.ssims.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ssims.common.BeanUtils;
import com.ssims.entity.Course;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName CourseVO
 * @Descrition Course视图对象，封装后端返回给前端的数据
 * @Author Jackie
 * @Date 2018/12/23 0:18
 */
@Data
@NoArgsConstructor
public class CourseVO {
    /**
     * id
     */
    private Long id;
    /**
     * 课程号
     */
    private String cno;
    /**
     * 课程名称
     */
    private String cname;
    /**
     * 学分
     */
    private Double credit;
    /**
     * 教师编号
     */
    private String teacherNo;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    public CourseVO(Course course) {
        BeanUtils.copyProperties(course, this);
    }

}
