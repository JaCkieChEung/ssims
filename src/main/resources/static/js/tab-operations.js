layui.define(['element', 'jquery'], function (exports) {
    var element = layui.element;
    var $ = layui.jquery;
    var tab = 'main-tab';

    //添加tab
    $('dl.layui-nav-child:not(.tab-tool-nav-child) dd').on('click', function () {
        var title = $(this).children('a').html() + '<i class="layui-icon layui-unselect layui-tab-close">ဆ</i>';
        var id = $(this).children('a').data('id');
        var content = '<iframe src="/' + id + '.html" id="' + id + '"></iframe>';
        var exist = false;
        //判断tab是否存在
        $('.layui-tab-title').find('li').each(function () {
            if ($(this).attr('lay-id') == id) {
                exist = true;
                return false;
            }
        });

        //不存在则添加
        if (!exist) { 
            element.tabAdd(tab, {
                title: title,
                content: content, //支持传入html
                id: id
            });
        } else {
            // 存在则刷新框架
            $('iframe#' + id).attr('src', $('iframe#' + id).attr('src'));
        }
        //切换tab
        element.tabChange(tab, id);
        //重新对tab选项卡进行初始化渲染
        element.render('tab', tab);
    });

    //删除tab
    $('body').on('click', '.layui-icon.layui-tab-close', function () {
        var id = $(this).parent('li').attr('lay-id');
        element.tabDelete(tab, id);
        if ($('.layui-tab .layui-tab-title').find('li').length == 0) {
            $('.layui-side .layui-nav-child').find('dd').removeClass('layui-this');
        }
    });

    //监听选项卡切换
    element.on('tab(main-tab)', function (data) {
        //var id = this.attributes["0"].nodeValue;
        var id = $(this).attr('lay-id');
        $('.layui-side .layui-nav-child').find('dd').removeClass('layui-this');
        $('.layui-side .layui-nav-child').find('a').each(function () {
            if ($(this).data('id') == id) {
                $(this).parent('dd').addClass('layui-this');
                return false;
            }
        });
    });

    exports('tab-operations', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});