package com.ssims.service;

import com.ssims.common.JsonResponse;
import com.ssims.entity.Student;
import com.ssims.model.StudentDTO;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName StudentService
 * @Descrition 学生业务逻辑接口
 * @Author Jackie
 * @Date 2018/12/22 15:56
 */
public interface StudentService extends BaseService<Student, Long> {
    /**
     * 根据学号查询学生
     *
     * @param num 学号
     * @return Optional<Student>
     */
    Optional<Student> findByNum(String num);

    /**
     * 新增或修改学生
     *
     * @param studentDTO Student数据传输对象
     * @return JsonResponse
     */
    JsonResponse addOrUpdateStu(StudentDTO studentDTO);

    /**
     * 查询所有学号
     *
     * @return List<Object>
     */
    List<Object> findAllSno();

}
