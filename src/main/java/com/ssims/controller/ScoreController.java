package com.ssims.controller;

import com.ssims.common.JsonResponse;
import com.ssims.common.PageableBuilder;
import com.ssims.entity.Score;
import com.ssims.model.ScoreDTO;
import com.ssims.model.ScoreVO;
import com.ssims.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

/**
 * @ClassName ScoreController
 * @Descrition Score控制器
 * @Author Jackie
 * @Date 2018/12/23 0:19
 */
@RestController
@RequestMapping("/score")
public class ScoreController {
    private final ScoreService scoreServiceImpl;

    @Autowired
    public ScoreController(ScoreService scoreServiceImpl) {
        this.scoreServiceImpl = scoreServiceImpl;
    }

    /**
     * 增加或修改成绩
     *
     * @param scoreDTO     ScoreDTO
     * @param bindingResult 验证结果
     * @return Map<String, Object>
     */
    @PostMapping("/save")
    public Map<String, Object> saveCourse(@RequestBody @Valid ScoreDTO scoreDTO, BindingResult bindingResult) {
        try {
            // 参数不合法，返回错误提示信息
            if (bindingResult.hasErrors()) {
                return JsonResponse.getErrorResult("参数不合法！" + bindingResult.getAllErrors().get(0).getDefaultMessage());
            } else {
                return scoreServiceImpl.addOrUpdateScore(scoreDTO).toMap();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("成绩保存失败！" + e.getMessage());
        }
    }

    /**
     * 查询所有成绩
     *
     * @param scoreDTO       ScoreDTO
     * @param pageableBuilder 分页构造器
     * @return Map<String, Object>
     */
    @GetMapping("/all")
    public Map<String, Object> findAllCourse(ScoreDTO scoreDTO, PageableBuilder pageableBuilder) {
        try {
            return JsonResponse.getSuccessResult(scoreServiceImpl.findAll(ScoreDTO.getScoreSpecification(scoreDTO), pageableBuilder).map(ScoreVO::new), "分页条件查询所有成绩成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("分页查询所有成绩失败！" + e.getMessage());
        }
    }

    /**
     * 查询成绩详情
     *
     * @param sId 成绩id
     * @return Map<String, Object>
     */
    @RequestMapping("/detail/{sId}")
    public Map<String, Object> findById(@PathVariable("sId") Long sId) {
        try {
            Optional<Score> optScore = scoreServiceImpl.findById(sId);
            if (optScore.isPresent()) {
                return JsonResponse.getSuccessResult(new ScoreVO(optScore.get()), "查询成绩详情成功！");
            } else {
                return JsonResponse.getErrorResult("该成绩不存在！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("查询成绩详情失败！" + e.getMessage());
        }
    }

    /**
     * 删除所选成绩
     *
     * @param ids 需要删除的成绩的id
     * @return Map<String, Object>
     */
    @RequestMapping("/delete")
    public Map<String, Object> deleteScore(@RequestParam(value="ids[]") Long[] ids) {
        try {
            scoreServiceImpl.deleteByIds(ids);
            return JsonResponse.getSuccessResult("所选成绩删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResponse.getErrorResult("所选成绩删除失败！" + e.getMessage());
        }
    }

    public ScoreService getScoreServiceImpl() {
        return scoreServiceImpl;
    }
}
