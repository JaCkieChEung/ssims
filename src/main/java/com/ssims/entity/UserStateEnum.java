package com.ssims.entity;

/**
 * @ClassName UserStateEnum
 * @Descrition 用户状态枚举类
 * @Author Jackie
 * @Date 2018/12/23 22:55
 */
public enum UserStateEnum {
    /**
     * 禁用
     */
    DISABLED,
    /**
     * 启用
     */
    ENABLED,
    /**
     * 无效
     */
    INVALID
}
