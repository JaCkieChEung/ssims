package com.ssims.service.impl;

import com.ssims.common.BeanUtils;
import com.ssims.common.JsonResponse;
import com.ssims.common.StringUtils;
import com.ssims.entity.Course;
import com.ssims.model.CourseDTO;
import com.ssims.model.CourseVO;
import com.ssims.repository.CourseRepository;
import com.ssims.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName CourseServiceImpl
 * @Descrition 课程业务逻辑接口实现类
 * @Author Jackie
 * @Date 2018/12/22 15:56
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CourseServiceImpl extends BaseServiceImpl<Course, Long> implements CourseService {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository) {
        super.setBaseRepository(courseRepository);
        this.courseRepository = courseRepository;
    }

    /**
     * 根据课程号查询课程
     *
     * @param cno 课程号
     * @return Optional<Student>
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Optional<Course> findByCno(String cno) {
        return courseRepository.findByCno(cno);
    }

    /**
     * 新增或修改课程
     *
     * @param courseDTO Course数据传输对象
     * @return JsonResponse
     */
    @Override
    public JsonResponse addOrUpdateCourse(CourseDTO courseDTO) {
        Long cid = courseDTO.getId();
        String cno = courseDTO.getCno();
        final Course[] course = new Course[1];
        JsonResponse result = new JsonResponse();

        if (StringUtils.isNotBlank(cno) && courseRepository.findByCno(cno).isPresent() && !courseRepository.findByCno(cno).get().getId().equals(cid)) {
            return result.buildErrorResult("课程号已存在！");
        }

        // id为空，新增课程
        if (cid == null) {
            course[0] = new Course();
        } else {
            if (!this.existsById(cid)) {
                return result.buildErrorResult("课程id不存在，课程信息修改失败！");
            } else {
                courseRepository.findById(cid).ifPresent(c -> course[0] = c);
            }
        }
        BeanUtils.copyProperties(courseDTO, course[0]);

        try {
            return result.buildSuccessResult(new CourseVO(courseRepository.save(course[0])), "课程信息保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return result.buildErrorResult(e.getMessage());
        }
    }

    /**
     * 查询所有课程号
     *
     * @return List<Object>
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Object> findAllCno() {
        return courseRepository.findAllCno();
    }

    public CourseRepository getCourseRepository() {
        return courseRepository;
    }
}
