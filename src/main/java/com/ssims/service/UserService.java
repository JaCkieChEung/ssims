package com.ssims.service;

import com.ssims.entity.User;

import java.util.Optional;

/**
 * @ClassName UserService
 * @Descrition 用户业务逻辑接口
 * @Author Jackie
 * @Date 2018/12/28 15:14
 */
public interface UserService extends BaseService<User, Long> {
    /**
     * 根据帐号和密码查找用户
     *
     * @param account  帐号
     * @param password 密码
     * @return Optional<User>
     */
    Optional<User> findByAccountAndPassword(String account, String password);
}
