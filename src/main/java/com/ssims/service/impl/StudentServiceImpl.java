package com.ssims.service.impl;

import com.ssims.common.BeanUtils;
import com.ssims.common.JsonResponse;
import com.ssims.common.StringUtils;
import com.ssims.entity.Score;
import com.ssims.entity.Student;
import com.ssims.model.StudentDTO;
import com.ssims.model.StudentVO;
import com.ssims.repository.ScoreRepository;
import com.ssims.repository.StudentRepository;
import com.ssims.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName StudentServiceImpl
 * @Descrition 学生业务逻辑接口实现类
 * @Author Jackie
 * @Date 2018/12/22 15:57
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StudentServiceImpl extends BaseServiceImpl<Student, Long> implements StudentService {

    private final StudentRepository studentRepository;
    private final ScoreRepository scoreRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, ScoreRepository scoreRepository) {
        super.setBaseRepository(studentRepository);
        this.studentRepository = studentRepository;
        this.scoreRepository = scoreRepository;
    }

    /**
     * 根据学号查询学生
     *
     * @param num 学号
     * @return Optional<Student>
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Optional<Student> findByNum(String num) {
        return studentRepository.findByNum(num);
    }

    /**
     * 新增或修改学生
     *
     * @param studentDTO Student数据传输对象
     * @return JsonResponse
     */
    @Override
    public JsonResponse addOrUpdateStu(StudentDTO studentDTO) {
        Long sid = studentDTO.getId();
        String number = studentDTO.getNum();
        final Student[] student = new Student[1];
        JsonResponse result = new JsonResponse();

        if (StringUtils.isNotBlank(number) && studentRepository.findByNum(number).isPresent() && !studentRepository.findByNum(number).get().getId().equals(sid)) {
            return result.buildErrorResult("学号已存在！");
        }

        // id为空，新增学生
        if (sid == null) {
            student[0] = new Student();
        } else {
            if (!this.existsById(sid)) {
                return result.buildErrorResult("学生id不存在，学生信息修改失败！");
            } else {
                studentRepository.findById(sid).ifPresent(s -> student[0] = s);
            }
        }
        BeanUtils.copyProperties(studentDTO, student[0]);

        if (studentDTO.getScoreIds() != null && !studentDTO.getScoreIds().isEmpty()) {
            List<Score> scores = scoreRepository.findAllById(studentDTO.getScoreIds());
            student[0].setScores(scores);
        }

        try {
            return result.buildSuccessResult(new StudentVO(studentRepository.save(student[0])), "学生信息保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return result.buildErrorResult(e.getMessage());
        }
    }

    /**
     * 查询所有学号
     *
     * @return List<Object>
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Object> findAllSno() {
        return studentRepository.findAllSno();
    }

    public StudentRepository getStudentRepository() {
        return studentRepository;
    }

    public ScoreRepository getScoreRepository() {
        return scoreRepository;
    }
}
