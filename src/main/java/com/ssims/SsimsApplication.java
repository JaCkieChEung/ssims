package com.ssims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsimsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsimsApplication.class, args);
    }

}

