package com.ssims.entity;

/**
 * @ClassName GenderEnum
 * @Descrition 性别枚举类
 * @Author Jackie
 * @Date 2018/12/21 15:35
 */
public enum GenderEnum {
    /**
     * 男
     */
    MALE,
    /**
     * 女
     */
    FEMALE
}
