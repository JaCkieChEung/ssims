package com.ssims.service.impl;

import com.ssims.entity.User;
import com.ssims.repository.UserRepository;
import com.ssims.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @ClassName UserServiceImpl
 * @Descrition 用户业务逻辑接口实现类
 * @Author Jackie
 * @Date 2018/12/28 15:14
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        super.setBaseRepository(userRepository);
        this.userRepository = userRepository;
    }

    /**
     * 根据帐号和密码查找用户
     *
     * @param account  帐号
     * @param password 密码
     * @return Optional<User>
     */
    @Override
    public Optional<User> findByAccountAndPassword(String account, String password) {
        return userRepository.findByAccountAndPassword(account, password);
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }
}
